using Grpc.Core;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace PersonService.Services
{
    public class PersonService : Person.PersonBase
    {
        const int NR_CARCTERE_CNP = 13;

        public PersonResponse.Types.Gender GetPersonGender(string cnp)
        {
            switch (cnp.ElementAt(0))
            {
                case '1':
                case '3':
                case '5':
                case '7':
                    return PersonResponse.Types.Gender.Male;

                case '2':
                case '4':
                case '6':
                case '8':
                    return PersonResponse.Types.Gender.Female;
            }
            throw new RpcException(new Status(StatusCode.InvalidArgument, "Sex Invalid!"));
        }

        public int GetPersonAge(string cnp)
        {
            Regex regex = new Regex(@"^[0-9]+$");

            if (cnp.ElementAt(0).Equals('9') || cnp.ElementAt(0).Equals('0') || !regex.IsMatch(cnp) || cnp.Length != NR_CARCTERE_CNP)
                throw new RpcException(new Status(StatusCode.InvalidArgument, "CNP invalid!"));

            System.Text.StringBuilder yearOfBirth = new System.Text.StringBuilder();
            System.Text.StringBuilder monthOfBirth = new System.Text.StringBuilder();
            System.Text.StringBuilder dayOfBirth = new System.Text.StringBuilder();
            switch (cnp.ElementAt(0))
            {
                case '1':
                case '2':
                case '7':
                case '8':
                    yearOfBirth.Append("19");
                    break;
                case '3':
                case '4':
                    yearOfBirth.Append("18");
                    break;
                case '5':
                case '6':
                    yearOfBirth.Append("20");
                    break;
            }

            yearOfBirth.Append(cnp.Substring(1, 2));
            monthOfBirth.Append(cnp.Substring(3, 2));
            dayOfBirth.Append(cnp.Substring(5, 2));

            DateTime birthdate = new DateTime();
            bool correctDate = DateTime.TryParse(string.Format("{0}-{1}-{2}",
                int.Parse(yearOfBirth.ToString()), int.Parse(monthOfBirth.ToString()), int.Parse(dayOfBirth.ToString())),
                out birthdate);

            if (!correctDate)
                throw new RpcException(new Status(StatusCode.InvalidArgument, "Data nastere invalida!"));

            var age = DateTime.Today.Year - birthdate.Year;

            // Go back to the year in which the person was born in case of a leap year
            if (birthdate.Date > DateTime.Today.AddYears(-age)) age--;

            return age;
        }

        public override Task<PersonResponse> GetPersonData(PersonRequest request, ServerCallContext context)
        {
            Regex regex = new Regex(@"^[a-z ,.'-]+$");
            if (!regex.IsMatch(request.Name))
                throw new RpcException(new Status(StatusCode.InvalidArgument, "Nume invalid!"));

            PersonResponse personResponse = new PersonResponse
            {
                Age = GetPersonAge(request.Cnp),
                Gender = GetPersonGender(request.Cnp)
            };

            Console.WriteLine("Nume: " + request.Name + "\nCNP: " + request.Cnp);

            return Task.FromResult(personResponse);
        }
    }
}
