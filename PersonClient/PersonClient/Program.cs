﻿using Grpc.Core;
using Grpc.Net.Client;
using System;
using System.Threading.Tasks;

namespace PersonClient
{
    class Program
    {
        static async Task Main(string[] args)
        {

            var channel = GrpcChannel.ForAddress("https://localhost:5001");
            var client = new Person.PersonClient(channel);

            bool toContinue = true;
            while (toContinue)
            {
                Console.WriteLine("Doriti sa trimiteti datele voastre personale catre server si sa primiti un raspuns? \n1.Apasati Y pentru DA\n2.Apasati N pentru NU");
                var choice = Console.ReadKey().Key;
                switch (choice)
                {
                    case System.ConsoleKey.Y:
                        Console.WriteLine("\nNumele:");
                        var name = Console.ReadLine();
                        Console.WriteLine("\nCNP:");
                        var cnp = Console.ReadLine();
                        try
                        {
                            var data = await client.GetPersonDataAsync(new PersonRequest { Name = name, Cnp = cnp });
                            Console.WriteLine("\n" + name + " are varsta de " + data.Age + " ani si e de sex " + data.Gender.ToString());
                        }
                        catch (RpcException e)
                        {
                            Console.WriteLine(e.Status.Detail);
                        }
                        toContinue = true;
                        break;

                    case System.ConsoleKey.N:
                        toContinue = false;
                        break;

                    default:
                        Console.WriteLine("\nTasta gresita!\n");
                        toContinue = true;
                        break;
                }
            }
        }
    }
}
